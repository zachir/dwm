/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static unsigned int borderpx  = 1;        /* border pixel of windows */
static unsigned int gappx     = 6;        /* gaps between windows */
static unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "mononoki Nerd Font Mono:size=12", "symbola:size=10" };
static const char dmenufont[]       = "mononoki Nerd Font Mono:size=12";
static char normbgcolor[]      = "#000000";
static char normbordercolor[]  = "#6e6f6c";
static char normfgcolor[]      = "#898883";
static char selfgcolor[]       = "#c5c3bc";
static char selbordercolor[]   = "#2783a1";
static char selbgcolor[]       = "#675f58";
static char urgfgcolor[]       = "#c5c3bc";
static char urgbordercolor[]   = "#2783a1";
static char urgbgcolor[]       = "#675f58";

static char *colors[][3]      = {
  /*                fg            bg            border                        */           
  [SchemeNorm] =  { normfgcolor,  normbgcolor,  normbordercolor }, // unfocused wins
  [SchemeSel] =   { selfgcolor,   selbgcolor,   selbordercolor }, // the focused win
  [SchemeUrg] =   { urgfgcolor,   urgbgcolor,   urgbordercolor },
};

typedef struct {
  const char *name;
  const void *cmd;
} Sp;
const char *spcmd0[] = { "st", "-g", "100x33", "-c", "sphtop", "-e", "htop", NULL };
const char *spcmd1[] = { "st", "-g", "100x33", "-c", "spterm", NULL };
const char *spcmd2[] = { "st", "-g", "100x33", "-c", "sppmxr", "-e", "pulsemixer", NULL };
const char *spcmd3[] = { "st", "-g", "100x33", "-c", "spblue", "-e", "bluetoothctl", NULL };
const char *spcmd4[] = { "st", "-g", "100x33", "-c", "spncmp", "-e", "ncmpcpp", NULL };
const char *spcmd5[] = { "st", "-g", "100x33", "-c", "spmutt", "-e", "zsh", "-c", "neomutt", NULL };
const char *spcmd6[] = { "st", "-g", "100x33", "-c", "spxmpp", "-e", "profanity", NULL };
const char *spcmd7[] = { "st", "-g", "100x33", "-c", "spircc", "-e", "irssi", NULL };
const char *spcmd8[] = { "st", "-g", "100x33", "-c", "sptodo", "-e", "todo", NULL };
const char *spcmd9[] = { "st", "-g", "100x33", "-c", "sptrmc", "-e", "tremc", NULL };
static Sp scratchpads[] = {
  { "sphtop",     spcmd0 },
  { "spterm",     spcmd1 },
  { "sppmxr",     spcmd2 },
  { "spblue",     spcmd3 },
  { "spncmp",     spcmd4 },
  { "spmutt",     spcmd5 },
  { "spxmpp",     spcmd6 },
  { "spircc",     spcmd7 },
  { "sptodo",     spcmd8 },
  { "sptrmc",     spcmd9 },
};

/* tagging */
static const char *tags[] = { " 1", " 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
        { NULL,      NULL,"Picture in picture", 511,      1,          0,          0,          -1 },
        { "ardour-6.2.0",NULL,  NULL,           0,        1,          0,          0,          -1 },
	{ "urxvt",	NULL,	NULL,		0,	  0,	      1,	  0,	      -1 },
	{ "URxvt",	NULL,	NULL,		0,	  0,	      1,	  0,	      -1 },
        { "Ardour-6.2.0",NULL,  NULL,           0,        1,          0,          0,          -1 },
	{ "Gimp",       NULL,   NULL,           0,        1,          0,          0,          -1 },
	{ "st-256color",NULL,   NULL,           0,        0,          1,          1,          -1 },
	{ "st",		NULL,   NULL,           0,        0,          1,          1,          -1 },
	{ "St",         NULL,   NULL,           0,        0,          1,          1,          -1 },
        { "tabbed",	NULL,   NULL,           0,        0,          1,          0,          -1 },
	{ NULL,     	NULL,   "abduco",       0,        0,          1,          0,          -1 },
        { "Alacritty",  NULL,   NULL,           0,        0,          1,          0,          -1 },
        { "Blueman",    NULL,   NULL,           0,        1,          0,          0,          -1 },
        { "QjackCtl",	NULL,   NULL,           0,        1,          0,          0,          -1 },
        { "qjackctl",	NULL,   NULL,           0,        1,          0,          0,          -1 },
        { "catia.py",	NULL,   NULL,           0,        1,          0,          0,          -1 },
        { "Catia",	NULL,   NULL,           0,        1,          0,          0,          -1 },
        { NULL, 	"carla",NULL,           1 << 7,   1,          0,          0,          -1 },
	{ NULL,		NULL,   "Event Tester", 0,        1,          0,          1,          -1 },
        { "Steam",	NULL,   NULL,           4,        0,          0,          0,          -1 },
        { "steam",	NULL,   NULL,           4,        0,          0,          0,          -1 },
        { NULL,		NULL,   "steam",        4,        0,          0,          0,          -1 },
        { "Lutris",	NULL,   NULL,           2,        0,          0,          0,          -1 },
        { "lutris",	NULL,   NULL,           2,        0,          0,          0,          -1 },
        { "sphtop",     NULL,   NULL,           SPTAG(0), 1,          1,          1,          -1 },
        { "spterm",     NULL,   NULL,           SPTAG(1), 1,          1,          1,          -1 },
        { "sppmxr",     NULL,   NULL,           SPTAG(2), 1,          1,          1,          -1 },
        { "spblue",     NULL,   NULL,           SPTAG(3), 1,          1,          1,          -1 },
        { "spncmp",     NULL,   NULL,           SPTAG(4), 1,          1,          1,          -1 },
        { "spmutt",     NULL,   NULL,           SPTAG(5), 1,          1,          1,          -1 },
        { "spxmpp",     NULL,   NULL,           SPTAG(6), 1,          1,          1,          -1 },
        { "spircc",     NULL,   NULL,           SPTAG(7), 1,          1,          1,          -1 },
        { "sptodo",     NULL,   NULL,           SPTAG(8), 1,          1,          1,          -1 },
        { "sptrmc",     NULL,   NULL,           SPTAG(9), 1,          1,          1,          -1 },
        { NULL, "monero-wallet-gui",NULL,       256,      1,          0,          0,          -1 },
        { "The Crown EX",NULL,  NULL,           0,        1,          0,          0,          -1 },
};

/* layout(s) */
static float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static int attachbelow = 1;    /* 1 means attach after the currently active window */

#include "tcl.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
        { "|||",      tcl },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
/* static const char *rmenucmd[] = { "/usr/sbin/j4-dmenu-desktop", NULL }; */
/* static const char *passmenu[] = { "passmenu", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL }; */
static const char *termcmd[]  = { "st", NULL };
/* static const char *termcmd[]  = { "alacritty", NULL }; */
static const char *sxhkdsig[] = { "doas", "pkill", "-HUP", "swhkd", NULL };
/* static const char *mpdtoggle[] = { "playerctl", "--player=mpd,mpv,%any", "play-pause", NULL }; */
/* static const char *mpdnext[] = { "playerctl", "--player=mpd,mpv,%any", "next", NULL }; */
/* static const char *mpdprev[] = { "playerctl", "--player=mpd,mpv,%any", "previous", NULL }; */
/* static const char *plytoggle[] = { "playerctl", "--player=%any,mpd", "play-pause", NULL }; */
/* static const char *plyfwd[] = { "playerctl", "--player=%any,mpd", "position 5+", NULL }; */
/* static const char *plybck[] = { "playerctl", "--player=%any,mpd", "position 5-", NULL }; */
/* static const char *blightup[] = { "light", "-A", "1", NULL }; */
/* static const char *blightdown[] = { "light", "-U", "1", NULL }; */
/* static const char *audioup[] = { "volsv", "-i", NULL }; */
/* static const char *audiodown[] = { "volsv", "-d", NULL }; */
/* static const char *audiomute[] = { "volsv", "-t", NULL }; */
/* static const char *micmute[] = { "pamixer", "--source", "1", "-t", NULL }; */
/* static const char *lockscr[] = { "xscreensaver-command", "-lock", NULL }; */
/* static const char *xidletog[] = { "xidletog", NULL }; */
/* static const char *xkillcmd[] = { "xkill", NULL }; */

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "normbgcolor",        STRING,  &normbgcolor },
		{ "normbordercolor",    STRING,  &normbordercolor },
		{ "normfgcolor",        STRING,  &normfgcolor },
		{ "selbgcolor",         STRING,  &selbgcolor },
		{ "selbordercolor",     STRING,  &selbordercolor },
		{ "selfgcolor",         STRING,  &selfgcolor },
		{ "urgbgcolor",         STRING,  &urgbgcolor },
		{ "urgbordercolor",     STRING,  &urgbordercolor },
		{ "urgfgcolor",         STRING,  &urgfgcolor },
		{ "borderpx",          	INTEGER, &borderpx },
		{ "gappx",          	INTEGER, &gappx },
		{ "snap",          	INTEGER, &snap },
		{ "showbar",          	INTEGER, &showbar },
		{ "topbar",          	INTEGER, &topbar },
		{ "nmaster",          	INTEGER, &nmaster },
		{ "resizehints",       	INTEGER, &resizehints },
		{ "mfact",      	FLOAT,   &mfact },
};

static Key keys[] = {
        /* modifier                     key        function        argument */
        /* { MODKEY,                       XK_d,       spawn,          {.v = dmenucmd } }, */
        /* { MODKEY,                       XK_r,       spawn,          {.v = rmenucmd } }, */
        /* { MODKEY,                       XK_p,       spawn,          {.v = passmenu } }, */
  	/* { MODKEY,                       XK_c,       spawn,          {.v = xidletog } }, */
        /* { MODKEY,                       XK_x,       spawn,          {.v = xkillcmd } }, */
        { MODKEY,                       XK_Return,  spawn,          {.v = termcmd } },
        { MODKEY|ControlMask,           XK_z,       togglescratch,  {.ui = 0 } },
        { MODKEY|ControlMask,           XK_x,       togglescratch,  {.ui = 1 } },
        { MODKEY|ControlMask,           XK_c,       togglescratch,  {.ui = 2 } },
        { MODKEY|ControlMask,           XK_v,       togglescratch,  {.ui = 3 } },
        { MODKEY|ControlMask,           XK_b,       togglescratch,  {.ui = 4 } },
        { MODKEY|ControlMask,           XK_a,       togglescratch,  {.ui = 5 } },
        { MODKEY|ControlMask,           XK_s,       togglescratch,  {.ui = 6 } },
        { MODKEY|ControlMask,           XK_d,       togglescratch,  {.ui = 7 } },
        { MODKEY|ControlMask,           XK_f,       togglescratch,  {.ui = 8 } },
        { MODKEY|ControlMask,           XK_g,       togglescratch,  {.ui = 9 } },
        { MODKEY,                       XK_Escape,  spawn,          {.v = sxhkdsig } },
     	// { 0,                     XF86XK_AudioPlay,  spawn,       {.v = mpdtoggle } },
  	// { 0,                     XF86XK_AudioNext,  spawn,       {.v = mpdnext } },
  	// { 0,                     XF86XK_AudioPrev,  spawn,       {.v = mpdprev } },
  	// { ShiftMask,             XF86XK_AudioPlay,  spawn,       {.v = plytoggle } },
  	// { ShiftMask,             XF86XK_AudioNext,  spawn,       {.v = plyfwd } },
  	// { ShiftMask,             XF86XK_AudioPrev,  spawn,       {.v = plybck } },
  	// { 0,               XF86XK_MonBrightnessUp,  spawn,       {.v = blightup } },
  	// { 0,             XF86XK_MonBrightnessDown,  spawn,       {.v = blightdown } },
  	// { 0,              XF86XK_AudioLowerVolume,  spawn,       {.v = audiodown } },
  	// { 0,              XF86XK_AudioRaiseVolume,  spawn,       {.v = audioup } },
  	// { 0,                     XF86XK_AudioMute,  spawn,       {.v = audiomute } },
  	// { 0,                  XF86XK_AudioMicMute,  spawn,       {.v = micmute } },
  	// { Mod4Mask,                     XK_l,       spawn,       {.v = lockscr } },
        { MODKEY,                       XK_b,       togglebar,      {0} },
        { MODKEY,                       XK_j,       focusstack,     {.i = +1 } },
        { MODKEY,                       XK_k,       focusstack,     {.i = -1 } },
        { MODKEY|ShiftMask,             XK_j,       pushdown,       {.i = +1 } },
        { MODKEY|ShiftMask,             XK_k,       pushup,         {.i = -1 } },
        { MODKEY|Mod4Mask,              XK_j,       pushmdown,       {.i = +1 } },
        { MODKEY|Mod4Mask,              XK_k,       pushmup,         {.i = -1 } },
	{ MODKEY|ControlMask,           XK_k,       setcfact,       {.f = +0.25} },
	{ MODKEY|ControlMask,           XK_j,       setcfact,       {.f = -0.25} },
	{ MODKEY|ControlMask,           XK_o,       setcfact,       {.f =  0.00} },
        { MODKEY|ShiftMask,             XK_h,       incnmaster,     {.i = +1 } },
        { MODKEY|ShiftMask,             XK_l,       incnmaster,     {.i = -1 } },
        { MODKEY,                       XK_h,       setmfact,       {.f = -0.05} },
        { MODKEY,                       XK_l,       setmfact,       {.f = +0.05} },
        { MODKEY|ShiftMask,             XK_Return,  zoom,           {0} },
        { MODKEY,                       XK_Tab,     view,           {0} },
        { MODKEY|ShiftMask,             XK_q,       killclient,     {0} },
        { MODKEY,                       XK_t,       setlayout,      {.v = &layouts[0]} },
        { MODKEY,                       XK_s,       setlayout,      {.v = &layouts[1]} },
        { MODKEY,                       XK_m,       setlayout,      {.v = &layouts[2]} },
        { MODKEY,                       XK_e,       setlayout,      {.v = &layouts[3]} },
        //{ MODKEY,                       XK_space,   setlayout,      {-1} },
        { MODKEY,                       XK_space,   togglefloating, {0} },
        { MODKEY,                       XK_f,       togglefullscr,  {0} },
        { MODKEY,                       XK_0,       view,           {.ui = ~0 } },
        { MODKEY|ShiftMask,             XK_0,       tag,            {.ui = ~0 } },
        { MODKEY,                       XK_comma,   focusmon,       {.i = -1 } },
        { MODKEY,                       XK_period,  focusmon,       {.i = +1 } },
        { MODKEY|ShiftMask,             XK_comma,   tagmon,         {.i = -1 } },
        { MODKEY|ShiftMask,             XK_period,  tagmon,         {.i = +1 } },
        TAGKEYS(                        XK_1,                      0)
        TAGKEYS(                        XK_2,                      1)
        TAGKEYS(                        XK_3,                      2)
        TAGKEYS(                        XK_4,                      3)
        TAGKEYS(                        XK_5,                      4)
        TAGKEYS(                        XK_6,                      5)
        TAGKEYS(                        XK_7,                      6)
        TAGKEYS(                        XK_8,                      7)
        TAGKEYS(                        XK_9,                      8)
        { MODKEY|ShiftMask,             XK_e,       quit,           {0} },
        { MODKEY|ShiftMask,             XK_r,       quit,           {1} },
        { MODKEY|ShiftMask,             XK_Tab,     toggleAttachBelow, {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
        { "togglescratch",  togglescratch },
	{ "focusstack",     focusstack },
	{ "setmfact",       setmfact },
	{ "togglebar",      togglebar },
	{ "incnmaster",     incnmaster },
	{ "togglefloating", togglefloating },
        { "togglefullscr",  togglefullscr },
	{ "focusmon",       focusmon },
	{ "tagmon",         tagmon },
	{ "zoom",           zoom },
	{ "view",           view },
	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "killclient",     killclient },
	{ "quit",           quit },
	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
};

